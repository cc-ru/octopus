use rlua::{Lua, Result};

pub struct Processor {
    lua: Lua
}

impl Processor {
    pub fn new() -> Self {
        Processor {
            lua: Lua::new()
        }
    }

    pub fn execute(&mut self, code: &str) -> Result<()> {
        self.lua.context(|ctx| {
            ctx.load(code).set_name("IRC input")?.exec()?;
            Ok(())
        })?;
        Ok(())
    }
}

