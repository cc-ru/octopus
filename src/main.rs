extern crate irc;
extern crate rlua;

mod processor;

use irc::client::prelude::*;
use processor::Processor;

fn send(client: &IrcClient, target: String, message: &str) {
    client.send(Command::PRIVMSG(target, message.to_owned())).unwrap()
}

fn main() {
    let prefix = ":";
    let config = Config {
        nickname: Some("octopus".to_owned()),
        alt_nicks: Some(vec!["octo".to_owned(), "kraken".to_owned()]),
        username: Some("octopus".to_owned()),
        realname: Some("octopus".to_owned()),
        user_info: Some("kawaii beast".to_owned()),
        source: Some("https://gitlab.com/UnicornFreedom/octopus".to_owned()),
        version: Some("0.1.0".to_owned()),
        ..
        Config::load("octopus.toml").unwrap()
    };

    let mut processor = Processor::new();

    let mut reactor = IrcReactor::new().unwrap();
    let client = reactor.prepare_client_and_connect(&config).unwrap();
    client.identify().unwrap();

    reactor.register_client_with_handler(client, move |client, message| {
        print!("{}", message);
        // filter private messages
        if let Command::PRIVMSG(ref target, ref msg) = message.command {
            // which start with the bot prefix
            if msg.starts_with(&prefix) {
                // and contain some useful data
                if let Some(data) = msg.get(1..) {
                    let mut parts = data.split_whitespace();
                    let name = parts.next().unwrap();
                    match name {
                        "cookie" => send(client, target.clone(), "thanks"),
                        "lua" => {
                            let code = data.get(name.len()..).unwrap();

                            match processor.execute(&code) {
                                Ok(()) =>
                                    send(client, target.clone(), "success"),
                                _ => send(client, target.clone(), "error")
                            }
                        },
                        _ => send(client, target.clone(), "wut")
                    }
                }
            }
        }
        Ok(())
    });

    reactor.run().unwrap();
}

